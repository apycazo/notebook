## Things to review

* [Constructor injection with lombok](https://www.baeldung.com/spring-injection-lombok)

## Hacks & tips

* [Embedded Jetty with JSP support](https://github.com/jetty-project/embedded-jetty-jsp)
